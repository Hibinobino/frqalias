﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FrqReader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;

        }

        private string ReadFrq(string filename)
        {
            double hz;
            string[] note = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    br.ReadBytes(12);
                    hz = br.ReadDouble();
                }

            }

            if (hz == 0.0) return "error";

            double rt12 = 1.0 / 12.0;
            double rt122 = Math.Pow(2, rt12);

            int key;

            if (442 > hz)
            {
                key = (int)Math.Log(442.0 / hz, rt122);
                return note[((45 - key) % 12)] + ((45 - key) / 12 + 1);
            }
            else
            {
                key = (int)Math.Log(hz / 442.0, rt122);
                return note[((45 + key) % 12)] + ((45 + key) / 12 + 1);
            }


        }
        private string ReadString(byte[] byt)
        {
            return Encoding.GetEncoding("shift-jis").GetString(byt);
        }
        private void Write(string a)
        {
            Console.Write(a);
        }
        private void WriteLine(string a)
        {
            Console.WriteLine(a);
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            for (int i = 0; i < files.Length; i++)
            {
                if (Path.GetFileName(files[i]) == "oto.ini")
                {
                    comboBox1.Enabled = false;
                    comboBox2.Enabled = false;
                    label1.ForeColor = Color.Gray;
                    ReadOto(files[i]);
                    comboBox1.Enabled = true;
                    comboBox2.Enabled = true;
                    label1.ForeColor = Color.Black;
                    MessageBox.Show("oto.iniを保存しました", "完了",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void ReadOto(string fullpath)
        {
            string directory = Path.GetDirectoryName(fullpath);
            Encoding[] enc = { Encoding.GetEncoding("shift-jis"), Encoding.UTF8 };
            string[] lines = File.ReadAllLines(fullpath, enc[comboBox1.SelectedIndex]);
            
            for (int x = 0; x < lines.Length; x++)
            {
                string[] sp = { "=", "," };
                string[] split = lines[x].Split(sp, StringSplitOptions.None);

                string frqname = Path.GetFileNameWithoutExtension(split[0]) + "_wav.frq";
                if (File.Exists(directory + "\\" + frqname) == false) continue;
                Console.WriteLine("Checking: " + Path.GetFileName(frqname));

                string note = ReadFrq(directory + "\\" + frqname);
                if (note == "error") continue;

                if (split[1] != "" && comboBox2.SelectedIndex == 1)
                {
                    split[1] += "-" + note;
                }
                else
                {
                    split[1] = Path.GetFileNameWithoutExtension(split[0]) + "-" + note;
                }
                WriteLine("Alias : " + split[1]);

                lines[x] = split[0] + "=" + split[1] + "," + split[2] + "," + split[3] + "," + split[4] + "," + split[5] + "," + split[6];
            }
            File.WriteAllLines(directory + "\\oto.ini", lines, enc[comboBox1.SelectedIndex]);

        }
    }
}
